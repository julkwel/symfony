<?php

namespace App\Front\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController
{
//    public function index()
//    {
//        return $this->render('api/index.html.twig', [
//            'controller_name' => 'ApiController',
//        ]);
//    }
    /**
     * @var integer HTTP status code -200 ( OK ) by default
     */
      protected $statusCode = 200;

      /**
       * Gets the value of statusCode
       * @return integer
       */
      public function getStatusCode()
      {
          return $this->statusCode;
      }

      /**
       * Sets the value of statusCode
       * @param integer $statusCode the status code
       *
       * @return self
       */
      protected function setStatusCode($statusCode)
      {
          $this->statusCode=$statusCode;
          return $this;
      }

      /**
       * Return a JSON response
       * @param array $data
       * @param array $header
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respond($data,$header = [] )
      {
          return new JsonResponse($data, $this->getStatusCode(),$header);
      }

      /**
       * Sets an error message and return a JSON response
       * @param string $errors
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respondWithErrors($errors,$header = [])
      {
          $data = [
              'errors' => $errors,
          ];
          return new JsonResponse($errors, $this->getStatusCode(),$header);
      }

      /**
       * Return a 401 unhotorized http response
       *
       * @param string $message
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respondUnauthorized($message = 'Not authorized!' )
      {
          return $this->setStatusCode(401)->respondWithErrors($message);
      }

      /**
       * Return a 402 Unprocessable Entity
       *
       * @param string $message
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respondValidationError($message = 'Validation Error')
      {
          return $this->setStatusCode(402)->respondWithErrors($message);
      }

      /**
       * Return a 404 not found
       *
       * @param string $message
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respondNotFound($message = 'Not found')
      {
          return $this->setStatusCode(404)->respondWithErrors($message);
      }

      /**
       * Return a 201 Created
       *
       * @param array $data
       *
       * @return Symfony\Component\HttpFoundation\JsonResponse
       */
      public function respondCreated($data = [])
      {
          return $this->setStatusCode(201)->respond($data);
      }

}

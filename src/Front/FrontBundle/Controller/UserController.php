<?php

namespace App\Front\FrontBundle\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class UserController extends ApiController
{

//    public function index()
//    {
//        return $this->render('user/index.html.twig', [
//            'controller_name' => 'UserController',
//        ]);
//    }
//    public function user()
//    {
//        return new JsonResponse([
//            [
//                'title'=> 'Hello user',
//                'count'=> 0
//            ]
//        ]);
//    }

    public function indexAction(UserRepository $userRepository)
    {
        $user = $userRepository->transformAll();
        return $this->respond($user);
    }

    /**
     * @Route("/users/{id}")
     * @Method("GET")
     */
    public function show($id,UserRepository $userRepository)
    {
        $user = $userRepository->find($id);

        if (! $user){
            return $this->respondNotFound();
        }

        $user = $userRepository->transform($user);

        return $this->respond($user);
    }

    /**
     * @Route("/users")
     * @Method("POST")
     */
    public function create(Request $request , UserRepository $userRepository ,EntityManagerInterface $em)
    {
        $request = $this->transformJsonBody($request);

        if (! $request){
            return $this->respondValidationError('Please provide a valid user');
        }

        if (! $request->get('name')){
            return $this->respondValidationError('Please provide a valid name');
        }

        $user = new User();
        $user->setName($request->get('name'));
        $user->setEmail('email');
        $em  ->persist($user);
        $em  ->flush();

        return $this->respondCreated($userRepository->transform($user));
    }

    /**
     * @Route("/Users/{id}/email")
     * @Method("POST")
     */
    public function increaseEmail($id ,EntityManagerInterface $em,UserRepository $userRepository)
    {
        $user = $userRepository->find($id);

        if(! $user){
            return $this->respondNotFound();
        }

        $user ->setEmail($user->getEmail());
        $em   ->persist($user);
        $em   ->flush();

        return $this->respond([
           'email' => $user->getEmail()
        ]);

    }


}

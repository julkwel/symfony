<?php
/**
 * Created by PhpStorm.
 * User: jul
 * Date: 8/3/18
 * Time: 2:55 PM
 */

namespace App\Front\FrontBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function homeAction(){
        return $this->render('Front/Home/Home.html.twig');
    }

}
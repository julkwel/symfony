<?php

namespace App\Front\FrontBundle\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CrudController extends Controller
{

    public function indexAction(UserRepository $userRepository)
    {
        $user = $userRepository->findAll();
        return $this->render('Front/Crud/index.html.twig',array(
            'users' => $user,
        ));
    }

    public function insertAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST'){
          $user = new User();
          $user->setName($request->request->get('name'));
          $user->setEmail($request->request->get('email'));
//          $user->setDescription($request->get('description'));
          $em->persist($user);
          $em->flush();
          return $this->redirectToRoute('crud');
        }
        return $this->render('Front/Form/NewUser.html.twig');
    }

    public function deleteAction($id,UserRepository $userRepository)
    {
        $user = $userRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('crud');
    }

    public function editAction($id,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if ($request->getMethod() == 'POST'){
            $user->setName($request->request->get('name'));
            $user->setEmail($request->request->get('email'));
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('crud');
        }

        return $this->render('Front/Form/EditUser.html.twig',[
            'user'  => $user,
        ]);

    }
}
